from collections import namedtuple
from io import BytesIO
from lxml import etree
import pkgutil

XMCDASchema = namedtuple('XMCDASchema', 'id url resource')

__base_url = 'http://www.decision-deck.org/xmcda/_downloads'
XMCDA_3_0_0 = XMCDASchema('http://www.decision-deck.org/2013/XMCDA-3.0.0',
                          f'{__base_url}/XMCDA-3.0.0.xsd',
                          'xsd/XMCDA-3.0.0.xsd')

XMCDA_3_0_1 = XMCDASchema('http://www.decision-deck.org/2016/XMCDA-3.0.1',
                          f'{__base_url}/XMCDA-3.0.1.xsd',
                          'xsd/XMCDA-3.0.1.xsd')

XMCDA_3_0_2 = XMCDASchema('http://www.decision-deck.org/2018/XMCDA-3.0.2',
                          f'{__base_url}/XMCDA-3.0.2.xsd',
                          'xsd/XMCDA-3.0.2.xsd')

XMCDA_3_1_0 = XMCDASchema('http://www.decision-deck.org/2018/XMCDA-3.1.0',
                          f'{__base_url}/XMCDA-3.1.0.xsd',
                          'xsd/XMCDA-3.1.0.xsd')

XMCDA_3_1_1 = XMCDASchema('http://www.decision-deck.org/2019/XMCDA-3.1.1',
                          f'{__base_url}/XMCDA-3.1.1.xsd',
                          'xsd/XMCDA-3.1.1.xsd')
del __base_url


XMCDA_xsds = (XMCDA_3_1_1, XMCDA_3_1_0, XMCDA_3_0_2, XMCDA_3_0_1, XMCDA_3_0_0)


def validate(xml, xmcda_schema):
    '''Validates a xml with the supplied XMCDASchema

    The xml can be either an ElementTree or an Element object
    '''
    xsd = BytesIO(pkgutil.get_data('xmcda', xmcda_schema.resource))
    xmlschema_doc = etree.parse(xsd, etree.XMLParser(no_network=True))
    xmlschema = etree.XMLSchema(xmlschema_doc)
    ret = xmlschema.validate(xml)
    return ret, xmlschema.error_log

def validateXMCDA(xml):
    for xml_schema in XMCDA_xsds:
        if validate(xml, xml_schema)[0]:
            return True
    return False


class AllTags:
    # all your tag are belong to us
    def __contains__(self, item):
        return True


ROOT_TAGS = ('alternatives',
             'alternativesSets',
             'criteria',
             'criteriaSets',
             'categories',
             'categoriesSets',

             'performanceTable',

             # data linked to alternatives

             'alternativesValues',
             'alternativesSetsValues',
             'alternativesLinearConstraints',
             'alternativesSetsLinearConstraints',
             'alternativesMatrix',
             'alternativesSetsMatrix',

             # data linked to criteria

             'criteriaFunctions',
             'criteriaScales',
             'criteriaThresholds',
             'criteriaValues',
             'criteriaSetsValues',
             'criteriaLinearConstraints',
             'criteriaSetsLinearConstraints',
             'criteriaMatrix',
             'criteriaSetsMatrix',

             # data linked to alternatives and criteria

             'alternativesCriteriaValues',

             # data linked to categories

             'categoriesProfiles',
             'alternativesAssignments',
             'categoriesValues',
             'categoriesSetsValues',
             'categoriesLinearConstraints',
             'categoriesSetsLinearConstraints',
             'categoriesMatrix',
             'categoriesSetsMatrix',

             # data linked to algorithms

             'programParameters',
             'programExecutionResult',
             )
