from .mixins import CommonAttributes, HasDescription
from .value import ValuedLabel, Value
from . import utils
from .utils import xfind, xfindall

from enum import Enum


class PreferenceDirection(Enum):
    MIN = 1
    MAX = 2

    @staticmethod
    def get(prefDir_str):
        try:
            return PreferenceDirection[prefDir_str.upper()]
        except Exception as e:
            raise KeyError(e)


class Scale(CommonAttributes):

    def merge_xml(self, element):
        CommonAttributes.merge_xml(self, element)

    @staticmethod
    def build(xml_element):
        if xfind(xml_element, './nominal') is not None:
            return NominalScale(xml_element)
        if xfind(xml_element, './qualitative') is not None:
            return QualitativeScale(xml_element)
        if xfind(xml_element, './quantitative') is not None:
            return QuantitativeScale(xml_element)
        raise ValueError(
            "Invalid xml element, cannot build a scale when neither nominal, "
            "qualitative or quantitative is found"
        )


class NominalScale(Scale):

    labels = ()

    def __init__(self, xml_element=None, **kw):
        self.labels = []
        if xml_element is not None:
            self.merge_xml(xml_element)
        for k, v in kw.items():
            setattr(self, k, v)

    def merge_xml(self, element):
        super().merge_xml(element)
        nominal_element = xfind(element, './nominal')
        if nominal_element is None:
            raise ValueError('Parameter element has no child <nominal/>')

        for label in xfindall(nominal_element, './labels/label'):
            self.labels.append(label.text)

    def to_xml(self):
        E = utils.element_maker()
        element = E.scale(utils.CommonAttributes_as_dict(self))
        nominal = E.nominal()
        labels = [E.label(label) for label in self.labels]
        labels = E.labels(*labels)
        nominal.append(labels)
        element.append(nominal)
        return element


class QualitativeScale(Scale):

    preference_direction = PreferenceDirection.MAX
    valued_labels = ()

    def __init__(self, xml_element=None, **kw):
        self.valued_labels = []
        if xml_element is not None:
            self.merge_xml(xml_element)
        for k, v in kw.items():
            setattr(self, k, v)

    def merge_xml(self, element):
        super().merge_xml(element)
        qualitative_elt = xfind(element, './qualitative')
        if qualitative_elt is None:
            raise ValueError('Parameter element has no child <qualitative/>')

        _prefDir_xpath = './qualitative/preferenceDirection'
        self.preference_direction = \
            PreferenceDirection.get(element.findtext(_prefDir_xpath))

        valued_labels = xfindall(qualitative_elt, './valuedLabels/valuedLabel')
        for valued_label in valued_labels:
            self.valued_labels.append(ValuedLabel(valued_label))

    def to_xml(self):
        E = utils.element_maker()
        element = E.scale(utils.CommonAttributes_as_dict(self))
        qualitative = E.qualitative()
        pref_dir = self.preference_direction.name.lower()
        qualitative.append(E.preferenceDirection(pref_dir))
        valued_labels = [vlabel.to_xml() for vlabel in self.valued_labels]
        if len(valued_labels) > 0:
            valued_labels = E.valuedLabels(*valued_labels)
            qualitative.append(valued_labels)
        element.append(qualitative)
        return element


class QuantitativeScale(Scale):

    preference_direction = PreferenceDirection.MAX
    minimum = maximum = None

    def __init__(self, xml_element=None, **kw):
        if xml_element is not None:
            self.merge_xml(xml_element)
        for k, v in kw.items():
            setattr(self, k, v)

    def merge_xml(self, element):
        super().merge_xml(element)
        quantitative_element = xfind(element, './quantitative')
        if quantitative_element is None:
            raise ValueError('Parameter element has no child <quantitative/>')

        _prefDir_xpath = './quantitative/preferenceDirection'
        self.preference_direction = \
            PreferenceDirection.get(element.findtext(_prefDir_xpath))

        minimum = xfind(quantitative_element, './minimum')
        if minimum is not None:
            self.minimum = Value(minimum)
        maximum = xfind(quantitative_element, './maximum')
        if maximum is not None:
            self.maximum = Value(maximum)

    def to_xml(self):
        E = utils.element_maker()
        element = E.scale(utils.CommonAttributes_as_dict(self))
        quantitative = E.quantitative()
        pref_dir = self.preference_direction.name.lower()
        quantitative.append(E.preferenceDirection(pref_dir))
        if self.minimum is not None:
            quantitative.append(self.minimum.to_xml('minimum'))
        if self.maximum is not None:
            quantitative.append(self.maximum.to_xml('maximum'))
        element.append(quantitative)
        return element


class Scales(list, HasDescription):

    def __init__(self, xml_element=None, **kw):
        if xml_element is not None:
            self.merge_xml(xml_element)
        for k, v in kw.items():
            setattr(self, k, v)

    def merge_xml(self, element):
        HasDescription.merge_xml(self, element)

        for scale in xfindall(element, './scale'):
            self.append(Scale.build(scale))

    def to_xml(self):
        E = utils.element_maker()
        scales = E.scales()
        if self.description is not None:
            scales.append(self.description.to_xml())
        for scale in self:
            scales.append(scale.to_xml())
        return scales
