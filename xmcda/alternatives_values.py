from .mixins import CommonAttributes, HasDescription
from .alternatives import Alternatives, Alternative
from .value import Values
from . import utils
from .utils import xfind, xfindall, xfindtext
from xmcda import TagInfo


class AlternativeValue(CommonAttributes, HasDescription):

    alternative = values = None

    def __init__(self, xml_element=None, xmcda=None, **kw):
        if xml_element is not None:
            self.merge_xml(xml_element, xmcda)
        for k, v in kw.items():
            setattr(self, k, v)

    def is_numeric(self):
        return self.values.is_numeric()

    def merge_xml(self, element, xmcda=None):
        CommonAttributes.merge_xml(self, element)
        HasDescription.merge_xml(self, element)

        alternatives = \
            xmcda.alternatives if xmcda is not None else Alternatives()

        alternativeID = xfindtext(element, './alternativeID')
        if alternativeID is not None:
            self.alternative = alternatives[alternativeID]

        values = xfind(element, './values')
        if values is not None:
            self.values = Values(values)

    def to_xml(self):
        E = utils.element_maker()
        attributes = utils.CommonAttributes_as_dict(self)
        alternative_value = E.alternativeValue(**attributes)
        if self.description is not None:
            alternative_value.append(self.description.to_xml())
        if self.alternative is not None:
            alternative_value.append(E.alternativeID(self.alternative.id))
        if self.values is not None and len(self.values) > 0:
            alternative_value.append(self.values.to_xml())
        return alternative_value


class AlternativesValues(list, CommonAttributes, HasDescription):

    @classmethod
    def tag_info(cls):
        return TagInfo('alternativesValues', 'alternatives_values_list', cls,
                       AlternativeValue)

    def __init__(self, xml_element=None, xmcda=None, **kw):
        if xml_element is not None:
            self.merge_xml(xml_element, xmcda)
        for k, v in kw.items():
            setattr(self, k, v)

    def alternatives(self):
        return [alternative_value.alternative for alternative_value in self]

    def is_numeric(self):
        return all(map(AlternativeValue.is_numeric, self))

    def merge_xml(self, element, xmcda=None):
        CommonAttributes.merge_xml(self, element)
        HasDescription.merge_xml(self, element)

        for alternative_value in xfindall(element, './alternativeValue'):
            self.append(AlternativeValue(alternative_value, xmcda))

    def to_xml(self):
        E = utils.element_maker()
        attributes = utils.CommonAttributes_as_dict(self)
        alternatives_values = E.alternativesValues(**attributes)
        if self.description is not None:
            alternatives_values.append(self.description.to_xml())
        for alternative_value in self:
            alternatives_values.append(alternative_value.to_xml())
        return alternatives_values

    def __getitem__(self, index):
        '''
        Returns the AlternativeValue corresponding to the index.
        Parameter index can be:
        - an integer: the index of the element to get from the list,
        - a Alternative: the alternative value for this alternative is returned

        - a string: the alternative value for the alternative with this id is
        returned
        '''
        type_error = 'AlternativesValue indices must be integers, Alternative or string, not %s'  # noqa

        if index is None:
            raise TypeError(type_error % 'NoneType')

        if isinstance(index, int):
            return super().__getitem__(index)

        if isinstance(index, Alternative):
            for alternativeValue in self:
                if alternativeValue.alternative == index:
                    return alternativeValue
            raise IndexError('No such alternativeValue')

        if isinstance(index, str):
            for alternativeValue in self:
                if alternativeValue.alternative.id == index:
                    return alternativeValue
            raise IndexError('No such alternativeValue')

        raise TypeError(type_error % type(index))

    def __setitem__(self, index, value):
        type_error = 'AlternativesValue indices must be integers, Alternative or string, not %s'  # noqa
        if index is None:
            raise TypeError(type_error % 'NoneType')

        if isinstance(index, int):
            return super().__setitem__(index, value)

        if isinstance(index, Alternative):
            for idx, alternativeValue in enumerate(self):
                if alternativeValue.alternative == index:
                    return super().__setitem__(idx, value)
            raise IndexError('No such alternativeValue')

        if isinstance(index, str):
            for idx, alternativeValue in enumerate(self):
                if alternativeValue.alternative.id == index:
                    return super().__setitem__(idx, value)
            raise IndexError('No such alternativeValue')
        raise TypeError(type_error % type(index))
