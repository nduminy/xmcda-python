from .criteria import Criteria
from .functions import Functions
from .mixins import CommonAttributes, HasDescription
import xmcda
from . import utils
from .utils import xfind, xfindall, xfindtext


class CriterionFunction(CommonAttributes, HasDescription):

    criterion = None
    functions = None

    def __init__(self, xml_element=None, xmcda=None, **kw):
        if xml_element is not None:
            self.merge_xml(xml_element, xmcda)
        for k, v in kw.items():
            setattr(self, k, v)

    def merge_xml(self, element, xmcda=None):
        CommonAttributes.merge_xml(self, element)
        HasDescription.merge_xml(self, element)

        criteria = xmcda.criteria if xmcda is not None else Criteria()
        self.criterion = criteria[xfindtext(element, 'criterionID')]
        self.functions = Functions(xfind(element, './functions'))

    def to_xml(self):
        E = utils.element_maker()

        attrs = utils.CommonAttributes_as_dict(self)
        criterionFunction_xml = E.criterionFunction(attrs)
        if self.description is not None:
            criterionFunction_xml.append(self.description.to_xml())
        if self.criterion is not None:
            criterionFunction_xml.append(E.criterionID(self.criterion.id))
        if self.functions is not None:
            criterionFunction_xml.append(self.functions.to_xml())
        return criterionFunction_xml


class CriteriaFunctions(list, CommonAttributes, HasDescription):

    @classmethod
    def tag_info(cls):
        return xmcda.TagInfo('criteriaFunctions', 'criteria_functions_list',
                             cls, CriterionFunction)

    def __init__(self, xml_element=None, xmcda=None, **kw):
        if xml_element is not None:
            self.merge_xml(xml_element, xmcda)
        for k, v in kw.items():
            setattr(self, k, v)

    def merge_xml(self, element, xmcda=None):
        CommonAttributes.merge_xml(self, element)
        HasDescription.merge_xml(self, element)

        for criterionFunction in xfindall(element, './criterionFunction'):
            self.append(CriterionFunction(criterionFunction, xmcda))

    def to_xml(self):
        E = utils.element_maker()
        cSs_xml = E.criteriaFunctions(utils.CommonAttributes_as_dict(self))
        if self.description is not None:
            cSs_xml.append(self.description.to_xml())
        for criterionFunction in self:
            cSs_xml.append(criterionFunction.to_xml())
        return cSs_xml
