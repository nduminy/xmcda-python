from .criteria import Criteria
from .thresholds import Thresholds
from .mixins import CommonAttributes, HasDescription
import xmcda
from . import utils
from .utils import xfind, xfindall, xfindtext


class CriterionThreshold(CommonAttributes, HasDescription):

    criterion = None
    thresholds = None

    def __init__(self, xml_element=None, xmcda=None, **kw):
        if xml_element is not None:
            self.merge_xml(xml_element, xmcda)
        for k, v in kw.items():
            setattr(self, k, v)

    def merge_xml(self, element, xmcda=None):
        CommonAttributes.merge_xml(self, element)
        HasDescription.merge_xml(self, element)

        criteria = xmcda.criteria if xmcda is not None else Criteria()
        criterionID = xfindtext(element, 'criterionID')
        if criterionID is not None:
            self.criterion = criteria[criterionID]
        self.thresholds = Thresholds(xfind(element, './thresholds'))

    def to_xml(self):
        E = utils.element_maker()

        _attrs = utils.CommonAttributes_as_dict(self)
        criterion_threshold_xml = E.criterionThreshold(_attrs)
        if self.description is not None:
            criterion_threshold_xml.append(self.description.to_xml())
        if self.criterion is not None:
            criterion_threshold_xml.append(E.criterionID(self.criterion.id))
        if self.thresholds is not None:
            criterion_threshold_xml.append(self.thresholds.to_xml())
        else:
            criterion_threshold_xml.append(Thresholds().to_xml())
        return criterion_threshold_xml


class CriteriaThresholds(list, CommonAttributes, HasDescription):

    @classmethod
    def tag_info(cls):
        return xmcda.TagInfo('criteriaThresholds', 'criteria_thresholds_list',
                             cls, CriterionThreshold)

    def __init__(self, xml_element=None, xmcda=None, **kw):
        if xml_element is not None:
            self.merge_xml(xml_element, xmcda)
        for k, v in kw.items():
            setattr(self, k, v)

    def merge_xml(self, element, xmcda=None):
        CommonAttributes.merge_xml(self, element)
        HasDescription.merge_xml(self, element)

        for criterionThreshold in xfindall(element, './criterionThreshold'):
            self.append(CriterionThreshold(criterionThreshold, xmcda))

    def to_xml(self):
        E = utils.element_maker()
        cSs_xml = E.criteriaThresholds(utils.CommonAttributes_as_dict(self))
        if self.description is not None:
            cSs_xml.append(self.description.to_xml())
        for criterionThreshold in self:
            cSs_xml.append(criterionThreshold.to_xml())
        return cSs_xml
