from .criteria import Criteria
from .scales import Scales
from .mixins import CommonAttributes, HasDescription
import xmcda
from . import utils
from .utils import xfind, xfindall, xfindtext


class CriterionScale(CommonAttributes, HasDescription):

    criterion = None
    scales = None

    def __init__(self, xml_element=None, xmcda=None, **kw):
        if xml_element is not None:
            self.merge_xml(xml_element, xmcda)
        for k, v in kw.items():
            setattr(self, k, v)

    def merge_xml(self, element, xmcda=None):
        CommonAttributes.merge_xml(self, element)
        HasDescription.merge_xml(self, element)

        criteria = xmcda.criteria if xmcda is not None else Criteria()
        criterionID = xfindtext(element, 'criterionID')
        if criterionID is not None:
            self.criterion = criteria[criterionID]
        self.scales = Scales(xfind(element, './scales'))

    def to_xml(self):
        E = utils.element_maker()

        critScale_xml = E.criterionScale(utils.CommonAttributes_as_dict(self))
        if self.description is not None:
            critScale_xml.append(self.description.to_xml())
        if self.criterion is not None:
            critScale_xml.append(E.criterionID(self.criterion.id))
        if self.scales is not None:
            critScale_xml.append(self.scales.to_xml())
        else:
            critScale_xml.append(Scales().to_xml())
        return critScale_xml


class CriteriaScales(list, CommonAttributes, HasDescription):

    @classmethod
    def tag_info(cls):
        return xmcda.TagInfo('criteriaScales', 'criteria_scales_list',
                             cls, CriterionScale)

    def __init__(self, xml_element=None, xmcda=None, **kw):
        if xml_element is not None:
            self.merge_xml(xml_element, xmcda)
        for k, v in kw.items():
            setattr(self, k, v)

    def merge_xml(self, element, xmcda=None):
        CommonAttributes.merge_xml(self, element)
        HasDescription.merge_xml(self, element)

        for criterionScale in xfindall(element, './criterionScale'):
            self.append(CriterionScale(criterionScale, xmcda))

    def to_xml(self):
        E = utils.element_maker()
        cSs_xml = E.criteriaScales(utils.CommonAttributes_as_dict(self))
        if self.description is not None:
            cSs_xml.append(self.description.to_xml())
        for criterionScale in self:
            cSs_xml.append(criterionScale.to_xml())
        return cSs_xml
