from .mixins import CommonAttributes, HasDescription
from .mcdaObjects__sets import McdaObjects_Sets, McdaObjects_Set
from .value import Values
from . import utils
from .utils import xfind, xfindall, xfindtext
from xmcda import TagInfo


class McdaObjects_SetValue(CommonAttributes, HasDescription):

    mcdaObjects__set = values = None

    def __init__(self, xml_element=None, xmcda=None, **kw):
        if xml_element is not None:
            self.merge_xml(xml_element, xmcda)
        for k, v in kw.items():
            setattr(self, k, v)

    def merge_xml(self, element, xmcda=None):
        CommonAttributes.merge_xml(self, element)
        HasDescription.merge_xml(self, element)

        mcdaObjects__sets = (
            xmcda.mcdaObjects__sets if xmcda is not None
            else McdaObjects_Sets())

        mcdaObjects__set_id = xfindtext(element, './mcdaObjects_SetID')
        if mcdaObjects__set_id is not None:
            self.mcdaObjects__set = mcdaObjects__sets[mcdaObjects__set_id]

        values = xfind(element, './values')
        if values is not None:
            self.values = Values(values)

    def to_xml(self):
        E = utils.element_maker()
        attributes = utils.CommonAttributes_as_dict(self)
        mcdaObjects__set_value = E.mcdaObjects_SetValue(**attributes)
        if self.description is not None:
            mcdaObjects__set_value.append(self.description.to_xml())
        if self.mcdaObjects__set is not None:
            xmcdaObjects_SetID = E.mcdaObjects_SetID(self.mcdaObjects__set.id)
            mcdaObjects__set_value.append(xmcdaObjects_SetID)
        if self.values is not None and len(self.values) > 0:
            mcdaObjects__set_value.append(self.values.to_xml())
        return mcdaObjects__set_value


class McdaObjects_SetsValues(list, CommonAttributes, HasDescription):

    @classmethod
    def tag_info(cls):
        return TagInfo('mcdaObjects_SetsValues',
                       'mcdaObjects__sets_values_list',
                       cls, McdaObjects_SetValue)

    def __init__(self, xml_element=None, xmcda=None, **kw):
        if xml_element is not None:
            self.merge_xml(xml_element, xmcda)
        for k, v in kw.items():
            setattr(self, k, v)

    def merge_xml(self, element, xmcda=None):
        CommonAttributes.merge_xml(self, element)
        HasDescription.merge_xml(self, element)

        for mcdaObjects__set_v in xfindall(element, './mcdaObjects_SetValue'):
            self.append(McdaObjects_SetValue(mcdaObjects__set_v, xmcda))

    def to_xml(self):
        E = utils.element_maker()
        attributes = utils.CommonAttributes_as_dict(self)
        mcdaObjects__sets_values = E.mcdaObjects_SetsValues(**attributes)
        if self.description is not None:
            mcdaObjects__sets_values.append(self.description.to_xml())
        for mcdaObjects__set_value in self:
            mcdaObjects__sets_values.append(mcdaObjects__set_value.to_xml())
        return mcdaObjects__sets_values

    def __getitem__(self, index):
        '''
        Returns the McdaObjects_SetValue corresponding to the index.
        Parameter index can be:
        - an integer: the index of the element to get from the list,
        - a McdaObjects_Set: the mcdaObjects_Set value for this
          mcdaObjects_Set is returned

        - a string: the mcdaObjects_Set value for the mcdaObjects_Set
          with this id is returned
        '''
        type_error = 'McdaObjects_SetsValue indices must be integers, McdaObjects_Set or string, not %s'  # noqa

        if index is None:
            raise TypeError(type_error % 'NoneType')

        if isinstance(index, int):
            return super().__getitem__(index)

        if isinstance(index, McdaObjects_Set):
            for mcdaObjects__set_value in self:
                if mcdaObjects__set_value.mcdaObjects__set == index:
                    return mcdaObjects__set_value
            raise IndexError('No such mcdaObjects_SetValue')

        if isinstance(index, str):
            for mcdaObjects__set_value in self:
                if mcdaObjects__set_value.mcdaObjects__set.id == index:
                    return mcdaObjects__set_value
            raise IndexError('No such mcdaObjects_SetValue')

        raise TypeError(type_error % type(index))

    def __setitem__(self, index, value):
        type_error = 'McdaObjects_SetsValue indices must be integers, McdaObjects_Set or string, not %s'  # noqa
        if index is None:
            raise TypeError(type_error % 'NoneType')

        if isinstance(index, int):
            return super().__setitem__(index, value)

        if isinstance(index, McdaObjects_Set):
            for idx, mcdaObjects__set_value in enumerate(self):
                if mcdaObjects__set_value.mcdaObjects__set == index:
                    return super().__setitem__(idx, value)
            raise IndexError('No such mcdaObjects_SetValue')

        if isinstance(index, str):
            for idx, mcdaObjects__set_value in enumerate(self):
                if mcdaObjects__set_value.mcdaObjects__set.id == index:
                    return super().__setitem__(idx, value)
            raise IndexError('No such mcdaObjects_SetValue')
        raise TypeError(type_error % type(index))
