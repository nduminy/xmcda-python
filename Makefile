
.PHONY: lint test coverage coverage-report coverage-report-html cov-html

lint:
	flake8 xmcda tests

test:
	python -m pytest

coverage:
	coverage run --source xmcda,tests --branch -m pytest

coverage-report: coverage
	coverage report

coverage-report-html cov-html: coverage
	coverage html
