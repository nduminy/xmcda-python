Read, write and manipulate XMCDA objects
========================================

This package enables the manipulating MCDA objects, as defined in the standard data interexchange format [XMCDA](https://www.decision-deck.org/xmcda/)

XMCDA is a data standard which allows to represent MultiCriteria Decision Analysis (MCDA) data elements in XML according to a clearly defined grammar, designed by the [Decision Deck Consortium](https://www.decision-deck.org/).
