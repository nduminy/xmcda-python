#! /bin/bash

script_dir="$(pwd)/${0%/*}"

declare -A tags
tags=(
  ["alternative"]="alternatives"
  ["criterion"]="criteria"
  ["category"]="categories"
)

# xxx_sets
for target in "${!tags[@]}" ; do
    tmpl='xmcda/xxx_sets.py.tmpl'
    target_file="${tmpl/xxx/${tags[$target]}}"  # replace xxx
    target_file="${target_file%.tmpl}" # remove trailing .tmpl
    cp "$tmpl" "${target_file}"
    sed -i -f "${script_dir}/to_${target}.sed" "${target_file}"
done

# tests/test_xxx_sets
for target in alternative criterion category ; do
    tmpl='tests/test_xxx_set.py.tmpl'
    target_file="${tmpl/xxx/${tags[$target]}}"  # replace xxx
    target_file="${target_file%.tmpl}" # remove trailing .tmpl
    cp "$tmpl" "${target_file}"
    sed -i -f "${script_dir}/to_${target}.sed" "${target_file}"
done

# xxx_value
for target in "${!tags[@]}" ; do
    tmpl='xmcda/xxx_values.py.tmpl'
    target_file="${tmpl/xxx/${tags[$target]}}"  # replace xxx
    target_file="${target_file%.tmpl}" # remove trailing .tmpl
    cp "$tmpl" "${target_file}"
    sed -i -f "${script_dir}/to_${target}.sed" "${target_file}"
done

# tests/test_xxx_values
for target in alternative criterion category ; do
    tmpl='tests/test_xxx_values.py.tmpl'
    target_file="${tmpl/xxx/${tags[$target]}}"  # replace xxx
    target_file="${target_file%.tmpl}" # remove trailing .tmpl
    cp "$tmpl" "${target_file}"
    sed -i -f "${script_dir}/to_${target}.sed" "${target_file}"
done

# xxx_sets_values
for target in "${!tags[@]}" ; do
    tmpl='xmcda/xxx_sets_values.py.tmpl'
    target_file="${tmpl/xxx/${tags[$target]}}"  # replace xxx
    target_file="${target_file%.tmpl}" # remove trailing .tmpl
    cp "$tmpl" "${target_file}"
    sed -i -f "${script_dir}/to_${target}.sed" "${target_file}"
done

# tests/test_xxx_sets_values
for target in "${!tags[@]}" ; do
    tmpl='tests/test_xxx_sets_values.py.tmpl'
    target_file="${tmpl/xxx/${tags[$target]}}"  # replace xxx
    target_file="${target_file%.tmpl}" # remove trailing .tmpl
    cp "$tmpl" "${target_file}"
    sed -i -f "${script_dir}/to_${target}.sed" "${target_file}"
done

# xxx_matrix
for target in "${!tags[@]}" ; do
    tmpl='xmcda/xxxs_matrix.py.tmpl'
    target_file="${tmpl/xxxs/${tags[$target]}}"  # replace xxx
    target_file="${target_file%.tmpl}" # remove trailing .tmpl
    cp "$tmpl" "${target_file}"
    sed -i -f "${script_dir}/to_${target}.sed" "${target_file}"
done
# tests/test_xxxs_matrix
for target in alternative criterion category ; do
    tmpl='tests/test_xxxs_matrix.py.tmpl'
    target_file="${tmpl/xxxs/${tags[$target]}}"  # replace xxx
    target_file="${target_file%.tmpl}" # remove trailing .tmpl
    cp "$tmpl" "${target_file}"
    sed -i -f "${script_dir}/to_${target}.sed" "${target_file}"
done

exit 0
