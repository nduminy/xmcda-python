# -*- coding: utf-8 -*-
from .utils import XMCDATestCase
from xmcda.XMCDA import XMCDA


class TestWriteXMCDA(XMCDATestCase):

    def _test_read_write_xml(self, xmcda_file, _type, tags=()):
        self.maxDiff = None
        from .utils import compact_xml, utf8_to_utf8
        with open(xmcda_file, 'r') as f:
            source = f.read()
        source = compact_xml(source)
        result = utf8_to_utf8(source, _type, tags=tags)
        self.assertEqual(source, result)

    def test_criteriaValues(self):
        self._test_read_write_xml('tests/files/criteriaValues.xml', XMCDA,
                                  tags=('criteriaValues',))

    def test_alternativesSets(self):
        self._test_read_write_xml('tests/files/alternativesSets.xml', XMCDA,
                                  tags=('alternativesSets',))

    def test_criteriaSets(self):
        self._test_read_write_xml('tests/files/criteriaSets.xml', XMCDA,
                                  tags=('criteriaSets',))

    def test_alternativesMatrix(self):
        self._test_read_write_xml('tests/files/alternativesMatrix.xml', XMCDA,
                                  tags=('alternativesMatrix',))

    def test_criteriaMatrix(self):
        self._test_read_write_xml('tests/files/criteriaMatrix.xml', XMCDA,
                                  tags=('criteriaMatrix',))

    def test_categoriesMatrix(self):
        self._test_read_write_xml('tests/files/categoriesMatrix.xml', XMCDA,
                                  tags=('categoriesMatrix',))

    def test_criteriaHierarchy(self):
        self._test_read_write_xml('tests/files/criteriaHierarchy.xml', XMCDA,
                                  tags=('criteriaHierarchy',))

    def test_criteriaSetsHierarchy(self):
        self._test_read_write_xml('tests/files/criteriaSetsHierarchy.xml',
                                  XMCDA, tags=('criteriaSetsHierarchy',))

    def test_criteriaSetsValues(self):
        self._test_read_write_xml('tests/files/criteriaSetsValues.xml',
                                  XMCDA, tags=('criteria', 'criteriaSets',
                                               'criteriaSetsValues'))

    def test_alternativesSetsValues(self):
        self._test_read_write_xml('tests/files/alternativesSetsValues.xml',
                                  XMCDA, tags=('alternatives',
                                               'alternativesSets',
                                               'alternativesSetsValues'))

    def test_categoriesSetsValues(self):
        self._test_read_write_xml('tests/files/categoriesSetsValues.xml',
                                  XMCDA, tags=('categories', 'categoriesSets',
                                               'categoriesSetsValues'))

    def test_criteriaFunctions(self):
        self._test_read_write_xml('tests/files/criteriaFunctions.xml', XMCDA,
                                  tags=('criteriaFunctions',))


# à la place de element.findall('./criterionValue')
# element.xpath('./*[local-name()="criterionValue"]'
