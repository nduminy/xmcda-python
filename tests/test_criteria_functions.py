from .utils import XMCDATestCase
import xmcda
from xmcda.functions import Functions
from xmcda.criteria_functions import CriterionFunction, CriteriaFunctions
from xmcda.XMCDA import XMCDA


class TestCriterionFunction(XMCDATestCase):

    def test_init_with_kw(self):
        criterion_function = CriterionFunction(id='cF', attr=77)
        self.assertEqual(criterion_function.id, 'cF')
        self.assertEqual(criterion_function.attr, 77)

    xml_1 = '''
        <criterionFunction id="cF1" name="cF1n" mcdaConcept="cF1m">
            <description><comment>bla</comment></description>
            <criterionID>o1</criterionID>
            <functions>
                <function>
                    <discrete>
                        <point>
                            <abscissa>
                                <real>0.0</real>
                            </abscissa>
                            <ordinate>
                                <real>0.0</real>
                            </ordinate>
                        </point>
                        <point>
                            <abscissa>
                                <real>21334.0</real>
                            </abscissa>
                            <ordinate>
                                <real>1.0</real>
                            </ordinate>
                        </point>
                    </discrete>
                </function>
            </functions>
      </criterionFunction>
'''

    xml_empty = '''
<criterionFunction>
    <criterionID>o1</criterionID>
    <functions/>
</criterionFunction>'''

    def test_from_xml(self):
        xml = self.read_xml(self.xml_1)
        criterion_function = CriterionFunction(xml)

        self.assertEqual(criterion_function.id, 'cF1')
        self.assertEqual(criterion_function.name, 'cF1n')
        self.assertEqual(criterion_function.mcda_concept, 'cF1m')
        self.assertEqual(criterion_function.criterion.id, 'o1')
        self.assertEqual(criterion_function.description.comment, 'bla')
        self.assertIsInstance(criterion_function.functions, Functions)

    def test_from_xml_with_xmcda(self):
        xmcda = XMCDA()
        xml = self.read_xml(self.xml_1)
        o1 = xmcda.criteria['o1']
        criterion_function = CriterionFunction(xml, xmcda)

        self.assertEqual(criterion_function.criterion, o1)

    def test_to_xml(self):
        self._test_to_xml(self.xml_1, CriterionFunction)
        self._test_to_xml(self.xml_empty, CriterionFunction)

        criterion_function = CriterionFunction()
        self.assertEqual(
            '<criterionFunction/>',
            xmcda.utils.tostring(criterion_function.to_xml()))


class TestCriteriaFunctions(XMCDATestCase):

    def test_init_with_kw(self):
        criteria_functions = CriteriaFunctions(id='cFs', attr=777)
        self.assertEqual(criteria_functions.id, 'cFs')
        self.assertEqual(criteria_functions.attr, 777)

    xml_1 = '''
    <criteriaFunctions id="cFs1" name="cFs1n" mcdaConcept="cFs1m">
        <description><comment>cFs1 comment</comment></description>
        <criterionFunction>
            <criterionID>o1</criterionID>
            <functions>
                <function>
                    <constant>
                        <integer>3210</integer>
                    </constant>
                </function>
            </functions>
        </criterionFunction>
        <criterionFunction>
            <criterionID>o2</criterionID>
            <functions>
                <function>
                    <constant>
                        <integer>2222</integer>
                    </constant>
                </function>
            </functions>
        </criterionFunction>
    </criteriaFunctions>
    '''

    xmcda_1 = f'<xmcda>{xml_1}</xmcda>'

    xmcda_criteria = '''
<xmcda>
    <criteria id="id1" name="n1" mcdaConcept="m1">
        <criterion id="o1" name="n01" />
        <criterion id="o2" name="n02"><active>false</active></criterion>
    </criteria>
</xmcda>
'''
    xml_empty = '<criteriaFunctions/>'

    def test_from_xml(self):
        xml = self.read_xml(self.xml_1)
        criteria_functions = CriteriaFunctions(xml)
        self.assertEqual(criteria_functions.id, 'cFs1')
        self.assertEqual(criteria_functions.name, 'cFs1n')
        self.assertEqual(criteria_functions.mcda_concept, 'cFs1m')

    def test_from_xml_with_xmcda(self):
        xmcda = XMCDA()
        xml = self.read_xml(self.xml_1)
        o1 = xmcda.criteria['o1']
        criteria_functions = CriteriaFunctions(xml, xmcda)

        self.assertEqual(criteria_functions[0].criterion, o1)

    def test_load_criteriaFunctions(self):
        from io import StringIO

        # load the criteriaFunctions, and the criteria afterwards
        xmcda = XMCDA()
        xmcda.load(binary_stream=StringIO(self.xmcda_1))
        self.assertEqual(len(xmcda.criteria), 2)

        o2 = xmcda.criteria['o2']
        self.assertEqual(o2.name, None)
        self.assertTrue(o2.active)

        xmcda.load(binary_stream=StringIO(self.xmcda_criteria))
        self.assertEqual(len(xmcda.criteria), 2)
        self.assertEqual(o2, xmcda.criteria['o2'])
        self.assertEqual(o2.name, 'n02')
        self.assertFalse(o2.active)

        # load the criteria, and the criteriaFunctions afterwards
        xmcda = XMCDA()
        xmcda.load(binary_stream=StringIO(self.xmcda_criteria))
        self.assertEqual(len(xmcda.criteria), 2)
        o2 = xmcda.criteria['o2']
        self.assertEqual(o2.name, 'n02')
        self.assertFalse(o2.active)

        xmcda.load(binary_stream=StringIO(self.xmcda_1))
        self.assertEqual(len(xmcda.criteria), 2)
        self.assertEqual(o2, xmcda.criteria['o2'])
        self.assertEqual(o2.name, 'n02')
        self.assertFalse(o2.active)

    def test_to_xml(self):
        self._test_to_xml(self.xml_1, CriteriaFunctions)
        self._test_to_xml(self.xml_empty, CriteriaFunctions)
