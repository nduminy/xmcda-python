from .utils import XMCDATestCase, compact_xml
import xmcda
from xmcda.XMCDA import XMCDA
from xmcda.alternatives import Alternatives, Alternative
from xmcda.criteria import Criteria, Criterion
from xmcda.categories import Categories, Category
from xmcda.performance_table import PerformanceTable


class TestXMCDA_defaults(XMCDATestCase):

    def setUp(self):
        self.xmcda = XMCDA()

    def test_alternatives(self):
        xmcda = self.xmcda
        self.assertIsNotNone(xmcda.alternatives)
        self.assertTrue(isinstance(xmcda.alternatives, Alternatives))
        xmcda.alternatives.append(Alternative(id='a1'))
        self.assertIsNotNone(xmcda.alternatives['a1'])

    def test_criteria(self):
        xmcda = self.xmcda
        self.assertIsNotNone(xmcda.criteria)
        self.assertTrue(isinstance(xmcda.criteria, Criteria))
        xmcda.criteria.append(Criterion(id='c01'))
        self.assertIsNotNone(xmcda.criteria['c01'])

    def test_categories(self):
        xmcda = self.xmcda
        self.assertIsNotNone(xmcda.categories)
        self.assertTrue(isinstance(xmcda.categories, Categories))
        xmcda.categories.append(Category(id='cat1'))
        self.assertIsNotNone(xmcda.categories['cat1'])

    def test_performance_table(self):
        xmcda = self.xmcda
        self.assertIsNotNone(xmcda.performance_tables)
        self.assertTrue(isinstance(xmcda.performance_tables, list))
        xmcda.categories.append(PerformanceTable(id='perfTab1'))
        self.assertIsNotNone(xmcda.categories['perfTab1'])

    def test_criteria_scales(self):
        xmcda = self.xmcda
        self.assertIsNotNone(xmcda.criteria_scales_list)
        self.assertTrue(isinstance(xmcda.criteria_scales_list, list))

    xml_1 = '''<xmcda><alternatives><alternative id="a1"/></alternatives>
<criteria><criterion id="c1"></criterion></criteria></xmcda>'''

    def test_merge_xml(self):
        xmcda = XMCDA()
        xml = self.read_xml(self.xml_1)
        xmcda.merge_xml(xml, Alternatives)
        self.assertEqual(len(xmcda.alternatives), 1)

        xmcda = XMCDA()
        xml = self.read_xml(self.xml_1)
        xmcda.merge_xml(xml, 'alternatives')
        self.assertEqual(len(xmcda.alternatives), 1)

        xmcda = XMCDA()
        xml = self.read_xml(self.xml_1)
        xmcda.merge_xml(xml, ('alternatives', Criteria))
        self.assertEqual(len(xmcda.alternatives), 1)
        self.assertEqual(len(xmcda.criteria), 1)

        xmcda = XMCDA()
        xml = self.read_xml(self.xml_1)
        xmcda.merge_xml(xml, (Category, Criterion))
        self.assertEqual(len(xmcda.alternatives), 0)
        self.assertEqual(len(xmcda.criteria), 0)

    def test_load_preconditions(self):
        xmcda = self.xmcda
        self.assertRaises(ValueError, xmcda.load)
        with self.assertRaises(ValueError):
            xmcda.load(filename='f.xml', binary_stream='fake stream supplied')

    empty_xmcda = compact_xml('''
<xmcda:XMCDA xmlns:xmcda="http://www.decision-deck.org/2019/XMCDA-3.1.1"/>''')

    xmcda_with_a1 = compact_xml('''
<xmcda:XMCDA xmlns:xmcda="http://www.decision-deck.org/2019/XMCDA-3.1.1">
    <alternatives><alternative id="a1"/></alternatives>
</xmcda:XMCDA>''')

    xmcda_with_a1_c1 = compact_xml('''
<xmcda:XMCDA xmlns:xmcda="http://www.decision-deck.org/2019/XMCDA-3.1.1">
    <alternatives><alternative id="a1"/></alternatives>
    <criteria><criterion id="c1"/></criteria>
</xmcda:XMCDA>''')

    def test_to_xml(self):
        self.maxDiff = None
        xmcda_tostr = xmcda.utils.tostring

        xml_str = xmcda_tostr(XMCDA().to_xml())
        self.assertEqual(xml_str, self.empty_xmcda)

        x = self.xmcda
        x.alternatives.append(Alternative(id='a1'))
        x.criteria.append(Criterion(id='c1'))
        xml_str = xmcda_tostr(x.to_xml())
        self.assertEqual(xml_str, self.xmcda_with_a1_c1)

        xml_str = xmcda_tostr(x.to_xml(tags="alternatives"))
        self.assertEqual(xml_str, self.xmcda_with_a1)

        xml_str = xmcda_tostr(x.to_xml(tags=Alternatives))
        self.assertEqual(xml_str, self.xmcda_with_a1)

        xml_str = xmcda_tostr(x.to_xml(tags=('alternatives', 'criteria')))
        self.assertEqual(xml_str, self.xmcda_with_a1_c1)

        xml_str = xmcda_tostr(x.to_xml(tags=(Alternatives, Criteria)))
        self.assertEqual(xml_str, self.xmcda_with_a1_c1)
