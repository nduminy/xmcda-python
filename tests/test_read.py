from .utils import XMCDATestCase
from xmcda.XMCDA import XMCDA


class TestReadXMCDA(XMCDATestCase):

    def test_load_1(self):
        xmcda = XMCDA()
        xmcda.load('tests/files/xmcda_1.xml')
        alternatives = xmcda.alternatives
        self.assertEqual(len(alternatives), 3)
        a01 = alternatives['a01']
        self.assertTrue(a01.is_real)
        self.assertTrue(a01.active)

    def test_load_2(self):
        xmcda = XMCDA()
        xmcda.load('tests/files/xmcda_2.xml')
        alternatives = xmcda.alternatives
        self.assertEqual(len(alternatives), 3)
        a01 = alternatives['a02']
        self.assertTrue(a01.is_real)
        self.assertFalse(a01.active)

    def test_load_two_performance_tables(self):
        xmcda = XMCDA()
        xmcda.load('tests/files/two_performance_tables.xml')
        perfTables = xmcda.performance_tables
        self.assertEqual(len(perfTables), 2)

    def test_read_specific_tags(self):
        xmcda = XMCDA()
        xmcda.load('tests/files/read_specific_tags_only.xml',
                   tags='performanceTable')
        from xmcda import create_on_access
        from xmcda import set_create_on_access
        ori = create_on_access()
        try:
            set_create_on_access(False)

            self.assertEqual(len(xmcda.performance_tables), 1)
            self.assertEqual(xmcda.alternatives['a01'].name, None)

            with self.assertRaises(IndexError):
                xmcda.alternatives['a02']
        finally:
            set_create_on_access(ori)
