from .utils import XMCDATestCase
import xmcda
from xmcda.scales import Scales
from xmcda.criteria_scales import CriterionScale, CriteriaScales
from xmcda.XMCDA import XMCDA


class TestCriterionScale(XMCDATestCase):

    xml_1 = '''
        <criterionScale id="cS1" name="cS1n" mcdaConcept="cS1m">
            <description><comment>meuh</comment></description>
            <criterionID>g1</criterionID>
            <scales>
                <scale>
                    <quantitative>
                        <preferenceDirection>min</preferenceDirection>
                    </quantitative>
                </scale>
                <scale>
                    <nominal>
                        <labels>
                            <label>piou-piou</label>
                        </labels>
                    </nominal>
                </scale>
            </scales>
        </criterionScale>
    '''

    def test_init_with_args(self):
        critScale = CriterionScale(id='cSid', attr=99)
        self.assertEqual(critScale.id, 'cSid')
        self.assertEqual(critScale.attr, 99)

    def test_from_xml(self):
        xml = self.read_xml(self.xml_1)
        critScale = CriterionScale(xml)

        self.assertEqual(critScale.id, 'cS1')
        self.assertEqual(critScale.name, 'cS1n')
        self.assertEqual(critScale.mcda_concept, 'cS1m')
        self.assertEqual(critScale.criterion.id, 'g1')
        self.assertIsInstance(critScale.scales, Scales)

    def test_from_xml_with_xmcda(self):
        xmcda = XMCDA()
        xml = self.read_xml(self.xml_1)
        g1 = xmcda.criteria['g1']
        critScale = CriterionScale(xml, xmcda)

        self.assertEqual(critScale.criterion, g1)

    xml_empty = '<criterionScale/>'
    xml_empty_serialized = '<criterionScale><scales/></criterionScale>'

    def test_to_xml(self):
        self._test_to_xml(self.xml_1, CriterionScale)

        r = CriterionScale(self.read_xml(self.xml_empty)).to_xml()
        self.assertEqual(xmcda.utils.tostring(r), self.xml_empty_serialized)

        # if scale is None: serialization works as if empty
        r = CriterionScale(self.read_xml(self.xml_empty))
        r.scales = None
        r = r.to_xml()
        self.assertEqual(xmcda.utils.tostring(r), self.xml_empty_serialized)


class TestCriteriaScales(XMCDATestCase):

    xml_1 = '''
        <criteriaScales id="cSs1" name="cSs1n" mcdaConcept="cSs1m">
            <description><comment>fsqd</comment></description>
            <criterionScale>
                <criterionID>g1</criterionID>
                <scales>
                    <scale>
                        <quantitative>
                            <preferenceDirection>min</preferenceDirection>
                        </quantitative>
                    </scale>
                </scales>
            </criterionScale>
            <criterionScale>
                <criterionID>g2</criterionID>
                <scales>
                    <scale>
                        <quantitative>
                            <preferenceDirection>max</preferenceDirection>
                        </quantitative>
                    </scale>
                </scales>
            </criterionScale>
        </criteriaScales>
    '''

    xmcda_1 = f'<xmcda>{xml_1}</xmcda>'

    xml_empty = '<criteriaScales/>'

    xmcda_criteria = '''
<xmcda>
    <criteria id="id1" name="n1" mcdaConcept="m1">
            <criterion id="g1" name="n01" />
            <criterion id="g2" name="n02"><active>false</active></criterion>
    </criteria>
</xmcda>
'''

    def test_init_with_args(self):
        critScales = CriteriaScales(id='cSsid', attr=999)
        self.assertEqual(critScales.id, 'cSsid')
        self.assertEqual(critScales.attr, 999)

    def test_from_xml(self):
        xml = self.read_xml(self.xml_1)
        critScales = CriteriaScales(xml)
        self.assertEqual(critScales.id, 'cSs1')
        self.assertEqual(critScales.name, 'cSs1n')
        self.assertEqual(critScales.mcda_concept, 'cSs1m')

    def test_from_xml_with_xmcda(self):
        xmcda = XMCDA()
        xml = self.read_xml(self.xml_1)
        g1 = xmcda.criteria['g1']
        critScales = CriteriaScales(xml, xmcda)

        self.assertEqual(critScales[0].criterion, g1)

    def test_load_criteriaScales(self):
        from io import StringIO

        # load the criteriaScales, and the criteria afterwards
        xmcda = XMCDA()
        xmcda.load(binary_stream=StringIO(self.xmcda_1))
        self.assertEqual(len(xmcda.criteria), 2)

        g2 = xmcda.criteria['g2']
        self.assertEqual(g2.name, None)
        self.assertTrue(g2.active)

        xmcda.load(binary_stream=StringIO(self.xmcda_criteria))
        self.assertEqual(len(xmcda.criteria), 2)
        self.assertEqual(g2, xmcda.criteria['g2'])
        self.assertEqual(g2.name, 'n02')
        self.assertFalse(g2.active)

        # load the criteria, and the criteriaScales afterwards
        xmcda = XMCDA()
        xmcda.load(binary_stream=StringIO(self.xmcda_criteria))
        self.assertEqual(len(xmcda.criteria), 2)
        g2 = xmcda.criteria['g2']
        self.assertEqual(g2.name, 'n02')
        self.assertFalse(g2.active)

        xmcda.load(binary_stream=StringIO(self.xmcda_1))
        self.assertEqual(len(xmcda.criteria), 2)
        self.assertEqual(g2, xmcda.criteria['g2'])
        self.assertEqual(g2.name, 'n02')
        self.assertFalse(g2.active)

    def test_to_xml(self):
        self._test_to_xml(self.xml_1, CriteriaScales)
        self._test_to_xml(self.xml_empty, CriteriaScales)
