from .utils import XMCDATestCase
from xmcda.container import Container


class TestContainer(XMCDATestCase):

    def test_remove(self):
        class E:
            def __init__(self, anID):
                self.id = anID

        c = Container()
        e1, e2 = E('1'), E('2')
        c.append(e1)
        c.append(e2)
        self.assertRaises(ValueError, c.remove, None)
        self.assertRaises(ValueError, c.remove, 'unknown id')

        c.remove('2')
        self.assertSequenceEqual(c, [e1])

        c.remove(e1)
        self.assertSequenceEqual(c, [])
