import xmcda.utils as U

import unittest
import re


def compact_xml(xml):
    '''
    Removes all uncessary whitespaces in the xml string supplied.
    Note that it removes tail attributes made of whitespaces only.
    '''
    xml = re.sub(r'>\s*<', '><', xml.strip().replace('>\n', '>'))
    return re.sub('<!--.*?-->', '', xml)


def utf8_to_element(xml_string, klass):
    element = U.read(xml_string, '.')[0]  # lxml.etree.Element
    o = klass()
    o.merge_xml(element)
    return o


def element_to_utf8(element, tags=(), **kw):
    from xmcda.XMCDA import XMCDA
    if type(element) is not XMCDA:
        tags = {}
    else:   # restrict the tags only when serializing the root element XMCDA
        tags = {'tags': tags}
    return U.tostring(element.to_xml(**tags), **kw)


def utf8_to_utf8(xml_string, klass, **kw):
    return element_to_utf8(utf8_to_element(xml_string, klass), **kw)


class XMCDATestCase(unittest.TestCase):

    def read_xml(self, xml_as_string):
        '''Parse the string and return the root XML element.  Fail if the xml
        does not contain one and only one root element.

        '''
        xml = U.read(xml_as_string, '.')
        self.assertEqual(len(xml), 1)
        return xml[0]

    def _test_to_xml(self, xml, _type):
        self.maxDiff = None
        from .utils import compact_xml, utf8_to_utf8
        source = compact_xml(xml)
        result = utf8_to_utf8(xml, _type)
        self.assertEqual(source, result)
