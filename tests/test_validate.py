# -*- coding: utf-8 -*-
from .utils import XMCDATestCase
from xmcda import schemas
from xmcda import utils

_dir = 'tests/files'


class TestXMLSchemaValidation(XMCDATestCase):

    def test_validate_against_a_specific_schema(self):
        xmcda = utils.read(open(f'{_dir}/sample-xmcda-3.1.1.xml', 'rb'))
        ret, error = schemas.validate(xmcda, schemas.XMCDA_3_1_1)
        self.assertTrue(ret, error)

    def test_validateXMCDA(self):
        xmcda = utils.read(open(f'{_dir}/two_performance_tables.xml', 'rb'))
        # self.assertTrue(schemas.validate(xmcda, schemas.XMCDA_3_1_1))
        self.assertTrue(schemas.validateXMCDA(xmcda))

        invalid = utils.read(open(f'{_dir}/sample-invalid-XMCDA.xml', 'rb'))
        ret, error = schemas.validate(invalid, schemas.XMCDA_3_1_1)
        self.assertFalse(ret, error)
        self.assertFalse(schemas.validateXMCDA(invalid))
