from .utils import XMCDATestCase
import xmcda
from xmcda.thresholds import Thresholds
from xmcda.thresholds import ConstantThreshold, AffineThreshold
from xmcda.criteria_thresholds import CriterionThreshold, CriteriaThresholds
from xmcda.XMCDA import XMCDA


class TestCriterionThreshold(XMCDATestCase):

    xml_1 = '''
        <criterionThreshold id="cS1" name="cS1n" mcdaConcept="cS1m">
            <description><comment>meuh</comment></description>
            <criterionID>g1</criterionID>
            <thresholds>
                <description><comment>thresholds desc</comment></description>
                <threshold id="indifference">
                    <constant>
                        <real>2.5</real>
                    </constant>
                </threshold>
                <threshold id="preference">
                    <affine>
                        <type>direct</type>
                        <slope>
                            <real>1.0</real>
                        </slope>
                        <intercept>
                            <integer>2</integer>
                        </intercept>
                    </affine>
                </threshold>
            </thresholds>
        </criterionThreshold>
    '''

    def test_init_with_kw(self):
        critThreshold = CriterionThreshold(id='cT', attr=4)
        self.assertEqual(critThreshold.id, 'cT')
        self.assertEqual(critThreshold.attr, 4)

    def test_from_xml(self):
        xml = self.read_xml(self.xml_1)
        critThreshold = CriterionThreshold(xml)

        self.assertEqual(critThreshold.id, 'cS1')
        self.assertEqual(critThreshold.name, 'cS1n')
        self.assertEqual(critThreshold.mcda_concept, 'cS1m')
        self.assertEqual(critThreshold.criterion.id, 'g1')
        self.assertIsInstance(critThreshold.thresholds, Thresholds)

        self.assertIsNotNone(critThreshold.thresholds.description)
        self.assertEqual(critThreshold.thresholds.description.comment,
                         "thresholds desc")

        ind = critThreshold.thresholds[0]
        pref = critThreshold.thresholds[1]
        self.assertIsInstance(ind, ConstantThreshold)
        self.assertIsInstance(pref, AffineThreshold)

    def test_from_xml_with_xmcda(self):
        xmcda = XMCDA()
        xml = self.read_xml(self.xml_1)
        g1 = xmcda.criteria['g1']
        critThreshold = CriterionThreshold(xml, xmcda)

        self.assertEqual(critThreshold.criterion, g1)

    xml_empty = '<criterionThreshold/>'

    xml_empty_serialized = '<criterionThreshold><thresholds/></criterionThreshold>'

    def test_to_xml(self):
        self._test_to_xml(self.xml_1, CriterionThreshold)

        r = CriterionThreshold(self.read_xml(self.xml_empty)).to_xml()
        self.assertEqual(xmcda.utils.tostring(r), self.xml_empty_serialized)

        # even if thresholds is None, serialize it the same way
        r = CriterionThreshold(self.read_xml(self.xml_empty))
        r.thresholds = None
        r = r.to_xml()
        self.assertEqual(xmcda.utils.tostring(r), self.xml_empty_serialized)


class TestCriteriaThresholds(XMCDATestCase):

    # no description in <thresholds> here: test for the description is
    # in TestCriterionThreshold
    xml_1 = '''
        <criteriaThresholds id="cSs1" name="cSs1n" mcdaConcept="cSs1m">
            <description><comment>blah</comment></description>
            <criterionThreshold>
                <criterionID>g1</criterionID>
                <thresholds>
                    <threshold id="indifference">
                        <constant>
                            <real>2.5</real>
                        </constant>
                    </threshold>
                </thresholds>
            </criterionThreshold>
            <criterionThreshold>
                <criterionID>g2</criterionID>
                <thresholds>
                    <threshold id="indifference">
                        <constant>
                            <real>20.0</real>
                        </constant>
                    </threshold>
                </thresholds>
            </criterionThreshold>
        </criteriaThresholds>
    '''

    xmcda_1 = f'<xmcda>{xml_1}</xmcda>'

    xmcda_criteria = '''
<xmcda>
    <criteria id="id1" name="n1" mcdaConcept="m1">
        <description><comment>crits comment</comment></description>
            <criterion id="g1" name="n01" />
            <criterion id="g2" name="n02"><active>false</active></criterion>
    </criteria>
</xmcda>
'''

    def test_init_with_kw(self):
        critThresholds = CriteriaThresholds(id='cTs', attr=44)
        self.assertEqual(critThresholds.id, 'cTs')
        self.assertEqual(critThresholds.attr, 44)

    def test_from_xml(self):
        xml = self.read_xml(self.xml_1)
        critThresholds = CriteriaThresholds(xml)
        self.assertEqual(critThresholds.id, 'cSs1')
        self.assertEqual(critThresholds.name, 'cSs1n')
        self.assertEqual(critThresholds.mcda_concept, 'cSs1m')
        self.assertIsNone(critThresholds[0].thresholds.description)

    def test_from_xml_with_xmcda(self):
        xmcda = XMCDA()
        xml = self.read_xml(self.xml_1)
        g1 = xmcda.criteria['g1']
        critThresholds = CriteriaThresholds(xml, xmcda)

        self.assertEqual(critThresholds[0].criterion, g1)

    def test_load_criteriaThresholds(self):
        from io import StringIO

        # load the criteriaThresholds, and the criteria afterwards
        xmcda = XMCDA()
        xmcda.load(binary_stream=StringIO(self.xmcda_1))
        self.assertEqual(len(xmcda.criteria), 2)

        g2 = xmcda.criteria['g2']
        self.assertEqual(g2.name, None)
        self.assertTrue(g2.active)

        xmcda.load(binary_stream=StringIO(self.xmcda_criteria))
        self.assertEqual(len(xmcda.criteria), 2)
        self.assertEqual(g2, xmcda.criteria['g2'])
        self.assertEqual(g2.name, 'n02')
        self.assertFalse(g2.active)

        # load the criteria, and the criteriaThresholds afterwards
        xmcda = XMCDA()
        xmcda.load(binary_stream=StringIO(self.xmcda_criteria))
        self.assertEqual(len(xmcda.criteria), 2)
        g2 = xmcda.criteria['g2']
        self.assertEqual(g2.name, 'n02')
        self.assertFalse(g2.active)

        xmcda.load(binary_stream=StringIO(self.xmcda_1))
        self.assertEqual(len(xmcda.criteria), 2)
        self.assertEqual(g2, xmcda.criteria['g2'])
        self.assertEqual(g2.name, 'n02')
        self.assertFalse(g2.active)

    xml_empty = '<criteriaThresholds/>'

    def test_to_xml(self):
        self._test_to_xml(self.xml_1, CriteriaThresholds)
        self._test_to_xml(self.xml_empty, CriteriaThresholds)
