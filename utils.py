from lxml import etree

#from enum import Enum

def xml_boolean(str):
    return str.lower() in ('true', '1')

op = { bool: xml_boolean }

noop=lambda x: x

class XMCDA:
    alternativesList=[]

class Alternatives:
    alternatives=[]

class Description:
    pass

def handle_common_attributes(obj, element):
    setattr(obj, 'id', element.get('id'))
    setattr(obj, 'name', element.get('name'))
    setattr(obj, 'mcda_concept', element.get('mcdaConcept'))
    return obj

def handle_description(obj, element):
    description = element.find('description')
    if description is not None:
        from .Description import Description
        setattr(obj, 'description', Description().from_xml(description))
    return obj
